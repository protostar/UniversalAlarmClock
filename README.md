# UniversalAlarmClock

Universal Windows Platform App for an alarm clock for Windows & Windows Phones.



This application is still in the early stages of development.  While the app can be installed and tested on any Windows 10 or Windows Mobile device, at this time it is recommended to wait for further development as many features are broken or only partially implemented.  At this stage, it is recommended to simply watch the repository.

If you are experienced in XAML and C#, then you are welcome to create pull requests and help develop the app.  At this time, my main focus is getting the application functioning as an alarm clock.