﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Universal_Alarm_Clock;
using System.Threading;
using Windows.UI.Notifications;
using Windows.Data.Xml.Dom;
using Microsoft.Toolkit.Uwp.Notifications;
using Windows.Storage;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Universal_Alarm_Clock
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Alarm_1 : Page
    {

        private string result1 = "Your alarm has already passed for today.";

        public bool alarm1ToggleBool = false;

        private bool mondayButton = false;
        private bool tuesdayButton = false;
        private bool wednesdayButton = false;
        private bool thursdayButton = false;
        private bool fridayButton = false;
        private bool saturdayButton = false;
        private bool sundayButton = false;

        DispatcherTimer Timer = new DispatcherTimer();

        public ToastContent toastContent;

        public Alarm_1()
        {
            this.InitializeComponent();

            Timer.Tick += OnTimerTick;
            Timer.Interval = new TimeSpan(0, 0, 0);
            Timer.Start();
        }

        private void OnTimerTick(object sender, object e)
        {
            toastContent = ToastNotification();

            if (Alarm1ToggleSwitch.IsOn && DateTime.Now.TimeOfDay >= Alarm1TimePicker.Time)
            {
                //  Uri baseUri = new Uri("ms-appx:///Assets/Alarmsounds/");

                // toastContent.Audio.Src = new Uri($"{0}", finalUri);

                    switch (DateTime.Now.DayOfWeek)
                    {
                        case DayOfWeek.Friday:
                            if (fridayButton == true)
                            {
                                DayButtonAlarmResult();
                            }

                            break;

                        case DayOfWeek.Monday:
                            if (mondayButton == true)
                            {
                                DayButtonAlarmResult();
                            }

                            break;

                        case DayOfWeek.Saturday:
                            if (saturdayButton == true)
                            {
                                DayButtonAlarmResult();
                            }
                            break;

                        case DayOfWeek.Sunday:
                            if (sundayButton == true)
                            {
                                DayButtonAlarmResult();
                            }

                            break;

                        case DayOfWeek.Thursday:
                            if (thursdayButton == true)
                            {
                                DayButtonAlarmResult();
                            }

                            break;

                        case DayOfWeek.Tuesday:
                            if (tuesdayButton == true)
                            {
                                DayButtonAlarmResult();
                            }

                            break;

                        case DayOfWeek.Wednesday:
                            if (wednesdayButton == true)
                            {
                                DayButtonAlarmResult();
                            }

                            break;

                        default:
                            break;
                    }

            }

        }
    
        private void Alarm1ToggleSwitch_Toggled(object sender, RoutedEventArgs e)
        {

            ToggleSwitch alarm1Toggle = sender as ToggleSwitch;

            if (alarm1Toggle.IsOn == true)
            {
                alarm1ToggleBool = true;    
            }

            else
            {
                alarm1ToggleBool = false;
                ResultText.Text = "Your alarm is not on.";
            }

            checkAlarmSet();

        }

        public void Alarm1TimePicker_TimeChanged(object sender, TimePickerValueChangedEventArgs e)
        {
            checkAlarmSet();
        }

        public void checkAlarmSet()
        {
            int hours = Alarm1TimePicker.Time.Hours;
            int minutes = Alarm1TimePicker.Time.Minutes;

            if (alarm1ToggleBool == false)
            {
                ResultText.Text = "Your alarm is not on.";
            }

            else
            {
                if (hours > 12)
                {
                    int halfHours = hours - 12;

                    ResultText.Text = string.Format("Your alarm is set for {0}:{1} PM.", halfHours, minutes);
                }

                else
                {
                    ResultText.Text = string.Format("Your alarm is set for {0}:{1} AM.", hours, minutes);
                }

            }
        } 

        public ToastContent ToastNotification()
        {

           // StorageFolder localFolder = ApplicationData.Current.LocalFolder;

            if (App.notificationSource == null)
            {
                return new ToastContent()
                {
                    Scenario = ToastScenario.Alarm,
                    Visual = new ToastVisual()
                    {
                        BindingGeneric = new ToastBindingGeneric()
                        {
                            Children =
                        {
                            new AdaptiveText()
                            {
                                Text = "Alarm 1"
                            },
                            new AdaptiveText()
                            {
                                Text = "It's time!"
                            },
                        }
                        }
                    },

                    Actions = new ToastActionsSnoozeAndDismiss()
                    {

                    },

                    Audio = new ToastAudio()
                    {
                        Loop = true,
                    }

                };
            }

          else
          {
              return new ToastContent()
              {
                  Scenario = ToastScenario.Alarm,
                  Visual = new ToastVisual()
                  {
                      BindingGeneric = new ToastBindingGeneric()
                      {
                          Children =
                      {
                          new AdaptiveText()
                          {
                              Text = "Alarm 1"
                          },
                          new AdaptiveText()
                          {
                              Text = "It's time!"
                          },
                      }
                      }
                  },

                  Actions = new ToastActionsSnoozeAndDismiss()
                  {

                  },

                  Audio = new ToastAudio()
                  {
                      Loop = true,

                     // Src = new Uri("{0}"),
                  }

              }; 
          } 


      }


        private void MondayButton_ClickEvent(object sender, RoutedEventArgs e)
        {
            /*  App.clickCount++;

              if (App.clickCount == 1)
              {
                  MondayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
              }

              else if (App.clickCount == 2)
              {
                  MondayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 70, 70, 70));

                  App.clickCount = 0;
              } */

            if (mondayButton != true)
            {
                mondayButton = true;

                MondayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
            }

            else
            {
                mondayButton = false;

                MondayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 30, 30, 30));
            }

        }

        private void TuesdayButton_Click(object sender, RoutedEventArgs e)
        {
            if (tuesdayButton != true)
            {
                tuesdayButton = true;

                TuesdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
            }

            else
            {
                tuesdayButton = false;

                TuesdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 30, 30, 30));
            }
        }

        private void WednesdayButton_Click(object sender, RoutedEventArgs e)
        {
            if (wednesdayButton != true)
            {
                wednesdayButton = true;

                WednesdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
            }

            else
            {
                wednesdayButton = false;

                WednesdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 30, 30, 30));
            }
        }

        private void ThursdayButton_Click(object sender, RoutedEventArgs e)
        {
            if (thursdayButton != true)
            {
                thursdayButton = true;

                ThursdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
            }

            else
            {
                thursdayButton = false;

                ThursdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 30, 30, 30));
            }
        }

        private void FridayButton_Click(object sender, RoutedEventArgs e)
        {
            if (fridayButton != true)
            {
                fridayButton = true;

                FridayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
            }

            else
            {
                fridayButton = false;

                FridayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 30, 30, 30));
            }
        }

        private void SaturdayButton_Click(object sender, RoutedEventArgs e)
        {
            if (saturdayButton != true)
            {
                saturdayButton = true;

                SaturdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
            }

            else
            {
                saturdayButton = false;

                SaturdayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 30, 30, 30));
            }
        }

        private void SundayButton_Click(object sender, RoutedEventArgs e)
        {
            if (sundayButton != true)
            {
                sundayButton = true;

                SundayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 255, 0, 0));
            }

            else
            {
                sundayButton = false;

                SundayButton.Background.SetValue(SolidColorBrush.ColorProperty, Windows.UI.Color.FromArgb(255, 30, 30, 30));
            }
        }

        private void DayButtonAlarmResult()
        {
            ToastNotificationManager.CreateToastNotifier()
                               .Show(new ToastNotification(toastContent.GetXml()));

            Alarm1ToggleSwitch.IsOn = false;

            ResultText.Text = result1;
        }
    }
}
