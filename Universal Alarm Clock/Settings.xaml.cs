﻿using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using static Universal_Alarm_Clock.Methods.SongList;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Universal_Alarm_Clock
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Settings : Page
    {
        private AlarmSongs alarmSongs = new AlarmSongs();

        private List<AlarmSongs> alarmSongsList = new List<AlarmSongs>();

        StorageFolder localFolder = ApplicationData.Current.LocalFolder;

        public UriKind fileLocation;

        public Settings()
        {
            this.InitializeComponent();

            try
            {
                // DirectoryInfo info = new DirectoryInfo(@"D:\");

                //  FileInfo[] Files = info.GetFiles("*.mp3");

                FileSongNames();
            }
            catch (Exception e)
            {

                ResultText.Text =  e.ToString();
            }

        }

        private async void FileSongNames()
        {
            StorageFolder appInstalledFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;

            StorageFolder assets = await appInstalledFolder.GetFolderAsync(@"Assets\AlarmSounds");

            IReadOnlyList<StorageFile> files = await assets.GetFilesAsync();

            //IReadOnlyList<StorageFile> files = await ApplicationData.Current.LocalFolder.GetFilesAsync();

            int fileNameCount = 1;

            foreach (var file in files)
            {
                MusicProperties songProperties = await file.Properties.GetMusicPropertiesAsync();

                if (songProperties.Title is string && string.IsNullOrEmpty((string)songProperties.Title) == false)
                {
                    alarmSongsList.Add(new AlarmSongs());

                    SongComboBox.Items.Add(songProperties.Title);

                    alarmSongs.Name = songProperties.Title;
                }

                else
                {
                    alarmSongsList.Add(new AlarmSongs { Name = "Alarm " + fileNameCount});

                    string newFileName = string.Format("Alarm " + fileNameCount);

                    SongComboBox.Items.Add(newFileName);
                    alarmSongs.Name = newFileName;
                    alarmSongs.FilePath = file.DisplayName;
                    fileNameCount++;
                }

                 alarmSongs.UriPath = new Uri(file.Path);
            }

            foreach (var item in alarmSongsList)
            {
               // ResultText.Text += item.Name.ToString();
            }
        }

        private void AddSongButton_Click(object sender, RoutedEventArgs e)
        {
            SongComboBox.Items.Clear();

            FilePicker();

            FileSongNames();
        }

        private async void FilePicker()
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.MusicLibrary;
            picker.FileTypeFilter.Add(".mp3");


            StorageFile file = await picker.PickSingleFileAsync();

             var songName = file.Name;

             string rootPath = Windows.ApplicationModel.Package.Current.InstalledLocation.Path;

             string alarmPath = rootPath + @"\Assets\AlarmSounds\";

             StorageFolder alarmFolder = await StorageFolder.GetFolderFromPathAsync(alarmPath);

           // GetAssetFolderPath();

            if (file != null)
            {
                StorageFile fileCopy = await file.CopyAsync(alarmFolder, string.Format(songName), NameCollisionOption.ReplaceExisting);

                //await fileCopy.MoveAsync(alarmFolder, string.Format(songName), NameCollisionOption.ReplaceExisting);

                SongComboBox.Items.Clear();

                FileSongNames();

            }

            else
            {

            }

        }

        private void SongComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // toastContent.AddAudio(new Uri("{0}", fileLocation));

            MediaPlayer mediaPlayer = new MediaPlayer();

            foreach (var item in alarmSongsList)
            {
               // if ((string)SongComboBox.SelectedItem == )
                {

                    mediaPlayer.Source = MediaSource.CreateFromUri(alarmSongs.UriPath);
                }
            }

        
            //mediaPlayer.Source = MediaSource.CreateFromUri(alarmSongs.UriPath);

            mediaPlayer.Play();

           // App.fileName = SongComboBox.SelectedItem.ToString();
        }

        private async void GetAssetFolderPath()
        {
            string rootPath = Windows.ApplicationModel.Package.Current.InstalledLocation.Path;

            string alarmPath = rootPath + @"\Assets\AlarmSounds\";

            StorageFolder alarmFolder = await StorageFolder.GetFolderFromPathAsync(alarmPath);
        }

    }
}
