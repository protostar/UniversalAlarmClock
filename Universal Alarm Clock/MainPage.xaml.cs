﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Universal_Alarm_Clock.Mvvm;
using Universal_Alarm_Clock.Services.NavigationService;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Universal_Alarm_Clock
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage(Frame frame)
        {
            this.InitializeComponent();
            this.ShellSplitView.Content = frame;

            var update = new Action(() =>
            {
                var type = frame.CurrentSourcePageType;
                foreach (var radioButton in AllRadioButtons(this))
                {
                    var target = radioButton.CommandParameter as NavType;
                    if (target == null)
                    {
                        continue;
                    }
                    radioButton.IsChecked = target.Type.Equals(type);
                }

                this.ShellSplitView.IsPaneOpen = false;
                this.BackCommand.RaiseCanExecuteChanged();
            });

            frame.Navigated += (s, e) => update();
            this.Loaded += (s, e) => update();
            this.DataContext = this;

        }

        Command _backCommand;
        public Command BackCommand { get { return _backCommand ?? (_backCommand = new Command(ExecuteBack, CanBack)); } }
        private bool CanBack()
        {
            var nav = (App.Current as App).NavigationService;
            return nav.CanGoBack;
        }

        private void ExecuteBack()
        {
           var nav = (App.Current as App).NavigationService;
           nav.GoBack();
        } 
        
        // menu

        Command _menuCommand;
        public Command MenuCommand { get { return _menuCommand ?? (_menuCommand = new Command(ExecuteMenu)); } }

        private void ExecuteMenu()
        {
            this.ShellSplitView.IsPaneOpen = !this.ShellSplitView.IsPaneOpen;
        }

        // nav

        Command<NavType> _navCommand;
        public Command<NavType> NavCommand { get { return _navCommand ?? (_navCommand = new Command<NavType>(ExecuteNav)); } }

        private void ExecuteNav(NavType navType)
        {
            var type = navType.Type;
            var nav = (App.Current as App).NavigationService;

            if (type.Equals(typeof(MainPage)))
            {
                nav.ClearHistory();
            }

            if (nav.CurrentPageType != null && nav.CurrentPageType != type)
            {
                nav.Navigate(type, navType.Parameter);
            }
        }
   

        public List<RadioButton> AllRadioButtons(DependencyObject parent)
        {
            var list = new List<RadioButton>();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                if (child is RadioButton)
                {
                    list.Add(child as RadioButton);
                    continue;
                }

                list.AddRange(AllRadioButtons(child));
            }

            return list;
        }

        private void DontCheck(object s, RoutedEventArgs e)
        {
            (s as RadioButton).IsChecked = false;
        }
    }


    public class NavType
    {
        public Type Type { get; set; }
        public string Parameter { get; set; }
    }

    
    }
