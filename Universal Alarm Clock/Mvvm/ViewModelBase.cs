﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Universal_Alarm_Clock.Services.NavigationService;
using Windows.UI.Xaml.Navigation;

namespace Universal_Alarm_Clock.Mvvm
{
    public abstract class ViewModelBase : BindableBase
    {
        public ViewModelBase()
        {
            if (!Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
                this.Dispatch = (App.Current as Common.Bootstrapper).Dispatch;
                this.NavigationService = (App.Current as Common.Bootstrapper).NavigationService;
            }
        }

        public Action<Action> Dispatch { get; private set; }
        public NavigationService NavigationService { get; private set; }

        public virtual void OnNavigatedTo(string parameter, NavigationMode mode, IDictionary<string, object> state) { }
        public virtual Task OnNavigatedFromAsync(IDictionary<string, object> state, bool suspending) { return Task.FromResult<object>(null); }
        public virtual void OnNavigatingFrom(Services.NavigationService.NavigatingEventArgs args) { }
    }
}
