﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Universal_Alarm_Clock.Common;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;

namespace Universal_Alarm_Clock
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>

    sealed partial class App : Bootstrapper
    {
        internal static int clickCount;

        protected internal static string fileName;

        internal static string notificationSource;

       // internal ObservableCollection<>

        public App() : base()
        {
            this.InitializeComponent();
        }

        public override Task OnInitializeAsync()
        {
            Window.Current.Content = new MainPage(this.RootFrame);
            return base.OnInitializeAsync();
        }

        public override Task OnStartAsync(StartKind startKind, IActivatedEventArgs args)
        {
            this.NavigationService.Navigate(typeof(Clock));
            return Task.FromResult<object>(null);
        }

    }
}
