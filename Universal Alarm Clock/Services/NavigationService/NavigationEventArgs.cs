﻿using System;
using Windows.UI.Xaml.Navigation;

namespace Universal_Alarm_Clock.Services.NavigationService
{
    public class NavigationEventArgs : EventArgs
    {
        public NavigationMode NavigationMode { get; set; }
        public string Parameter { get; set; }
    }
}
