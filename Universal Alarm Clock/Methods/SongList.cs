﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Universal_Alarm_Clock.Methods
{
    public class SongList
    {
       public class AlarmSongs
            {

            public string Name { get; set; }

            public Uri UriPath { get; set; }

            public string FilePath { get; set; }

            }
    }
}
