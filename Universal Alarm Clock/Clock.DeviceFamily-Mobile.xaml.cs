﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Universal_Alarm_Clock
{
    public sealed partial class Clock : Page
    {
        


    
        private void Timer_Tick(object sender, object e)
        {

            MyClock.Text = DateTime.Now.ToString("h:mm");
        }

        private void Alarm2Button_ClickEvent(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Alarm_2));
        }

        private void Alarm1Button_ClickEvent(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Alarm_1));
        }

        private void Alarm3Button_ClickEvent(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Alarm3));
        }
    }
}
